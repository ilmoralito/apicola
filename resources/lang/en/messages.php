<?php

return [
    // navbar
    'about' => 'About',
    'hive' => 'Hive',
    'beehive' => 'Beehive',
    'getting_started' => 'Getting started',
    'financial_analysis' => 'Financial analysis',
    'catalog' => 'Catalog',
    'services' => 'Projects',
    'contact' => 'Contact us',
    'news' => 'News',

    // buttons
    'btn_blog' => 'Read more',
    'btn_catalog' => 'Detail',
    'btn_contact_us' => 'Send',
    'btn_buy_now' => 'Buy Now',
    'btn_add_to_car' => 'Add to Cart',
    'btn_go_back_post' => 'Go back',
    'btn_go_back_item' => 'Go back',
    'go_to_contact' => 'Continue',

    // blog
    'banner' => 'Hello, Welcome',
    'banner_content' => 'In this area you can inform about the various activities we do , besides publishing content that will help you to understand that beekeeping is . Hopefully you enjoy our little Blog.',
    'by' => 'By',

    // mision vision
    'mission' => 'Mission',
    'mission_content' => 'We are a company dedicated to research, innovation and development of apiculture products quality. We design, manufacture and market model hexagonal hives Vincent, equipment and accessories for beekeeping in order to meet the highest expectations of our customers. We are highly committed to environmental conservation, restoration of ecosystems, and social - economic development of beekeeping.',
    'view' => 'View',
    'view_content' => 'Becoming a leader in the manufacture of hives and distribution and customer service are always looking for excellence and leadership in beekeeping.',

    // info
    'direction' => 'Address',
    'phones' => 'Phones',

    // form contact us
    'title_panel' => 'Contact us',
    'name' => 'Name',
    'mail' => 'Mail',
    'message' => 'Message',
    'greeting' =>  'Thanks for writing to us. Soon we will contact you.',

    // services
    'courses' => 'Courses',
    'consultation' => 'Consultation',
    'development_projects' => 'Development projects',
    'manual' => 'Download manual',

    // activities
    'activities' => 'Activities',

    // items
    'description' => 'Description',
    'price' => 'Price'
];
