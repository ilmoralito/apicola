<?php

return [
    // navbar
    'about' => 'Nosotros',
    'hive' => 'Colmena',
    'beehive' => 'La colmena',
    'getting_started' => 'Empezando',
    'financial_analysis' => 'Analisis finaciero',
    'catalog' => 'Catalogo',
    'services' => 'Proyectos',
    'contact' => 'Contactanos',
    'news' => 'Noticias',

    // buttons
    'btn_blog' => 'Seguir leyendo',
    'btn_catalog' => 'Detalle',
    'btn_contact_us' => 'Enviar',
    'btn_buy_now' => 'Comprar ahora',
    'btn_add_to_car' => 'Añadir al carro de compras',
    'btn_go_back_post' => 'Volver',
    'btn_go_back_item' => 'Volver',
    'go_to_contact' => 'Continuar',

    // blog
    'banner' => 'Hola, Bienvenido',
    'banner_content' => 'En esta area podras informarte acerca de las diversas actividades que realizamos, ademas de publicar contenidos que te ayudaran a entender de que se trata la apicultura. Esperemos que disfrutes de nuestro pequeño Blog.',
    'by' => 'Por',

    // mision vision
    'mission' => 'Mision',
    'mission_content' => 'Somos un programa dedicado a la investigación, innovación y desarrollo tecnológicos de productos en el sector apícola. Diseñamos, fabricamos colmenas hexagonales modelo Vincent, basados en el hábitat natural de las abejas. Estamos altamente comprometidos con la conservación del ambiente, la restauración de los ecosistemas, y el desarrollo social - económico de la apicultura promoviendo un modelo productivo apícola sostenible.',
    'view' => 'Vision',
    'view_content' => 'Convertirse en un líder en la fabricación de productos de colmenas y la distribución y servicio al cliente que siempre están buscando la excelencia y el liderazgo en la apicultura.',

    // info
    'direction' => 'Direccion',
    'phones' => 'Telefonos',

    // form contact us
    'title_panel' => 'Contactanos',
    'name' => 'Nombre',
    'mail' => 'Correo',
    'message' => 'Mensaje',
    'greeting' =>  'Gracias por escribirnos, pronto te contactaremos.',

    // services
    'courses' => 'Cursos',
    'consultation' => 'Asesoria',
    'development_projects' => 'Proyectos en desarrollo',
    'manual' => 'Descarga de manual',

    // activities
    'activities' => 'Actividades',

    // items
    'description' => 'Descripcion',
    'price' => 'Precio'
];
